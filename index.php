<?php
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');
require_once "assets/database/db.php";

$sett = mysqli_fetch_object(mysqli_query($con, "select * from th_setting"));

if (isset($_POST['send'])) {
  $name = $_POST['fullname'];
  $no_ic = $_POST['no_ic'];
  $contact = $_POST['contact'];
  $email = $_POST['email'];
  $address = $_POST['address'];
  $date_in = $_POST['date_in'];
  $date_out = $_POST['date_out'];
  $days = $_POST['days'];
  $pax = $_POST['pax'];

  $msg = "*Desa Tunas Hijau* - Jalan Sungai Pagar%0A87000 W.P. Labuan%0A";
  $msg .= "*REGISTRATION FORM*%0A%0A";
  $msg .= "• *PERSONAL INFORMATION*%0A";
  $msg .= "Full Name : " . $name . "%0A";
  $msg .= "IC No. / Passport No. : " . $no_ic . "%0A";
  $msg .= "Contact No. : " . $contact . "%0A";
  $msg .= "E-mail : " . $email . "%0A";
  $msg .= "Address : " . $address . "%0A";
  $msg .= "%0A• *ACCOMMODATION*%0A";
  $msg .= "Date / Time - Check In : " . $date_in . "%0A";
  $msg .= "Date / Time - Check Out : " . $date_out . "%0A";
  $msg .= "No. of Day(s) : " . $days . "%0A";
  $msg .= "No. of Pax : " . $pax . "%0A";

  $useragent = $_SERVER['HTTP_USER_AGENT'];
  if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {

    echo "<script>window.open('whatsapp://send/?phone=$sett[whatsapp_number]&text=$msg','_blank')</script>";
  } else {
    echo "<script>window.open('https://web.whatsapp.com/send?text=$msg&phone=$sett[whatsapp_number]','_blank')</script>";
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!--====== Required meta tags ======-->
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

  <!--====== Title ======-->
  <title><?= $sett->title_tab ?></title>

  <!--====== Favicon Icon ======-->
  <link rel="shortcut icon" href="assets/images/favicon.svg" type="image/svg" />

  <!--====== Bootstrap css ======-->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />

  <!--====== Line Icons css ======-->
  <link rel="stylesheet" href="assets/css/lineicons.css" />

  <!--====== Tiny Slider css ======-->
  <link rel="stylesheet" href="assets/css/tiny-slider.css" />

  <!--====== gLightBox css ======-->
  <link rel="stylesheet" href="assets/css/glightbox.min.css" />

  <link rel="stylesheet" href="assets/css/style.css" />

  <!-- Added -->
  <link rel="stylesheet" href="assets/css/my-css.css">
  <!-- datepicker -->
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
  <!-- timepicker -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

  <style>
    .nav-text {
      font-weight: 700;
      color: var(--white);
      text-shadow: 0px 3px 8px #00000017;
      text-transform: capitalize;
    }

    .my-table>tbody>tr>td {
      padding: 0.2rem 0.2rem;
    }
  </style>
</head>

<body>

  <!-- Modal Booking -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel">REGISTRATION FORM</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="" method="post">
          <div class="modal-body">
            <div class="row">
              <!-- KIRI -->
              <div class="col-md-6">
                <h6 class="head-info">PERSONAL INFORMATION</h6>
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">Full Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="fullname">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">IC No. / Passport No.</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="no_ic">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">Contact No.</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="contact">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">E-mail</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="email">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">Address</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" name="address"></textarea>
                  </div>
                </div>
                <h6 class="head-info">PAYMENT METHOD</h6>
                <div class="mb-3 row">
                  <label class="col-sm-4 col-form-label">Deposit</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="deposit">
                  </div>
                  <!-- <div class="col-sm-4">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="cnkTransfer">
                      <label class="form-check-label" for="cnkTransfer">
                        Online Transfer
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="chkCash">
                      <label class="form-check-label" for="chkCash">
                        Cash
                      </label>
                    </div>
                  </div> -->
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-6">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="cnkTransfer2">
                      <label class="form-check-label" for="cnkTransfer2">
                        Online Transfer
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="chkCash2">
                      <label class="form-check-label" for="chkCash2">
                        Cash
                      </label>
                    </div>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-6">
                    <p style="color: #000; font-size: 0.875em; text-align: center;">
                      <b>PUBLIC BANK</b><br>
                      <b><?= $sett->bank_name ?></b><br>
                      <b><?= $sett->bank_no ?></b>
                    </p>
                  </div>
                  <div class="col-sm-6">
                    <div style="background-color: #dddddd; padding: 2px 7px">
                      <span style="font-size: 13px; font-weight: 500; text-align: justify; color: #000;">Please send copy of payment receipt to person incharge at Desa Tunas Hijau</span>
                    </div>
                  </div>
                </div>
                <h6 class="head-info">PAYMENT</h6>
                <div class="mb-3 row">
                  <label class="col-sm-6 col-form-label" style="text-align: end; font-size: 14px; font-weight: 600;">TOTAL AMOUNT OF ACCOMMODATION (RM)</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="email" readonly placeholder="0" style="background-color: #fff9ca;">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-6 col-form-label" style="text-align: end; font-size: 14px; font-weight: 600;">TOTAL AMOUNT OF ADDITIONAL (RM)</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="email" readonly placeholder="0" style="background-color: #fff9ca;">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-6 col-form-label" style="text-align: end; font-size: 14px; font-weight: 600;">GRAND TOTAL (RM)</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="email" readonly placeholder="0" style="background-color: #fff9ca;">
                  </div>
                </div>
              </div>

              <!-- KANAN -->
              <div class="col-md-6">
                <h6 class="head-info">ACCOMMODATION</h6>
                <div class="mb-3 row">
                  <label class="col-sm-5 col-form-label">Date / Time - Check In</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="date_in" placeholder="mm/dd/yyyy">
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control timepicker" name="time_in" placeholder="H:m">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-5 col-form-label">Date / Time - Check Out</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="date_out" placeholder="mm/dd/yyyy">
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control timepicker" name="time_out" placeholder="H:m">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-5 col-form-label">No. of Day(s)</label>
                  <div class="col-sm-7">
                    <input type="number" min="0" max="500" class="form-control" name="days">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-5 col-form-label">No. of Pax</label>
                  <div class="col-sm-7">
                    <input type="number" min="0" max="500" class="form-control" name="pax">
                  </div>
                </div>
                <table class="table my-table mb-20">
                  <tr>
                    <td><b>Type of Chalet</b></td>
                    <td align="center"><b>WEEKDAY(s)</b></td>
                    <td align="center"><b>WEEKEND(s)/P.H.</b></td>
                  </tr>
                  <?php
                  $no = 1;
                  $type = mysqli_query($con, "select * from th_list_type where active='Y'");
                  while ($r = mysqli_fetch_object($type)) {
                  ?>
                    <tr>
                      <td><?= $r->category ?></td>
                      <td align="right">RM<?= $r->weekday ?><input class="form-check-input ml-5" type="radio" name="type" value="<?= $r->id ?>-wd-<?= $r->weekday ?>" id="type<?= $no ?>"></td>
                      <td align="right">RM<?= $r->weekend ?><input class="form-check-input ml-5" type="radio" name="type" value="<?= $r->id ?>-we-<?= $r->weekend ?>" id="type<?= $no ?>"></td>
                    </tr>
                  <?php
                    $no++;
                  }
                  ?>
                </table>
                <h6 class="head-info">ADDITIONAL</h6>
                <div class="mb-3">
                  <?php
                  $no = 1;
                  $adds = mysqli_query($con, "select * from th_list_additional where active='Y'");
                  while ($r = mysqli_fetch_object($adds)) {
                  ?>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" value="" id="adds<?= $no ?>">
                      <label class="form-check-label" for="adds<?= $no ?>"><?= $r->category ?></label>
                    </div>
                  <?php
                    $no++;
                  }
                  ?>
                </div>
                <table class="table">
                  <tr>
                    <td class="no-border"><b>TAPAK PERKEMAHAN</b></td>
                    <td class="no-border"><b>:</b>
                      <?php
                      $perkemahan = mysqli_query($con, "select * from th_perkemahan where active='Y'");
                      while ($r = mysqli_fetch_object($perkemahan)) {
                        echo 'RM' . $r->price . '<i>(' . $r->category . ')</i>&nbsp;&nbsp;&nbsp;';
                      }
                      ?>
                    </td>
                  </tr>
                </table>

              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Close</button>
            <button type="submit" name="send" class="btn btn-success btn-sm">Save & Send</button>
            <!-- <input type="submit" name="submit" value="Save & Send" class="btn btn-success btn-sm"> -->
          </div>
        </form>
      </div>
    </div>
  </div>

  <!--====== NAVBAR NINE PART START ======-->

  <section class="navbar-area navbar-nine">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="index.html">
              <!-- <img src="assets/images/logo.jpg" alt="Logo" /> -->
              <h5 class="nav-text">DESA TUNAS HIJAU</h5>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNine" aria-controls="navbarNine" aria-expanded="false" aria-label="Toggle navigation">
              <span class="toggler-icon"></span>
              <span class="toggler-icon"></span>
              <span class="toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse sub-menu-bar" id="navbarNine">
              <ul class="navbar-nav me-auto">
                <li class="nav-item">
                  <a class="page-scroll active" href="#hero-area">Home</a>
                </li>
                <li class="nav-item">
                  <a class="page-scroll" href="#services">Services</a>
                </li>

                <li class="nav-item">
                  <a class="page-scroll" href="#pricing">Pricing</a>
                </li>
                <li class="nav-item">
                  <a class="page-scroll" href="#contact">Contact</a>
                </li>
              </ul>
            </div>

            <div class="navbar-btn d-none d-lg-inline-block">
              <a class="menu-bar" href="#side-menu-left"><i class="lni lni-menu"></i></a>
            </div>
          </nav>
          <!-- navbar -->
        </div>
      </div>
      <!-- row -->
    </div>
    <!-- container -->
  </section>

  <!--====== NAVBAR NINE PART ENDS ======-->

  <!--====== SIDEBAR PART START ======-->

  <div class="sidebar-left">
    <div class="sidebar-close">
      <a class="close" href="#close"><i class="lni lni-close"></i></a>
    </div>
    <div class="sidebar-content">
      <div class="sidebar-logo">
        <a href="index.html"><img src="assets/images/logo.svg" alt="Logo" /></a>
      </div>
      <p class="text">Lorem ipsum dolor sit amet adipisicing elit. Sapiente fuga nisi rerum iusto intro.</p>
      <!-- logo -->
      <div class="sidebar-menu">
        <h5 class="menu-title">Quick Links</h5>
        <ul>
          <li><a href="javascript:void(0)">About Us</a></li>
          <li><a href="javascript:void(0)">Our Team</a></li>
          <li><a href="javascript:void(0)">Latest News</a></li>
          <li><a href="javascript:void(0)">Contact Us</a></li>
        </ul>
      </div>
      <!-- menu -->
      <div class="sidebar-social align-items-center justify-content-center">
        <h5 class="social-title">Follow Us On</h5>
        <ul>
          <li>
            <a href="javascript:void(0)"><i class="lni lni-facebook-filled"></i></a>
          </li>
          <li>
            <a href="javascript:void(0)"><i class="lni lni-twitter-original"></i></a>
          </li>
          <li>
            <a href="javascript:void(0)"><i class="lni lni-linkedin-original"></i></a>
          </li>
          <li>
            <a href="javascript:void(0)"><i class="lni lni-youtube"></i></a>
          </li>
        </ul>
      </div>
      <!-- sidebar social -->
    </div>
    <!-- content -->
  </div>
  <div class="overlay-left"></div>

  <!--====== SIDEBAR PART ENDS ======-->

  <!-- Start header Area -->
  <section id="hero-area" class="header-area header-eight">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 col-md-12 col-12">
          <div class="header-content">
            <h1>WELCOME to DESA TUNAS HIJAU</h1>
            <p>
              also known as DESALAND. We offer eco adventure activities and accommodations for the young and the young-at-heart as well as fun activities for families and children.
            </p>
            <div class="button">
              <button type="button" class="btn primary-btn" data-bs-toggle="modal" data-bs-target="#exampleModal">Booking</button>
              <!-- <a href="https://www.youtube.com/watch?v=r44RKWyfcFw&fbclid=IwAR21beSJORalzmzokxDRcGfkZA1AtRTE__l5N4r09HcGS5Y6vOluyouM9EM"
                class="glightbox video-button">
                <span class="btn icon-btn rounded-full">
                  <i class="lni lni-play"></i>
                </span>
                <span class="text">Watch Intro</span>
              </a> -->
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-12 col-12">
          <div class="header-image">
            <img src="assets/images/header/image-2.jpg" alt="#" />
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End header Area -->

  <!--====== ABOUT FIVE PART START ======-->

  <section class="about-area about-five">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 col-12">
          <div class="about-image-five">
            <svg class="shape" width="106" height="134" viewBox="0 0 106 134" fill="none" xmlns="http://www.w3.org/2000/svg">
              <circle cx="1.66654" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="1.66654" cy="132" r="1.66667" fill="#DADADA" />
              <circle cx="16.3333" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="16.3333" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="16.3333" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="16.3333" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="16.333" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="16.333" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="16.333" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="16.333" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="16.333" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="16.333" cy="132" r="1.66667" fill="#DADADA" />
              <circle cx="30.9998" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="74.6665" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="30.9998" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="74.6665" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="30.9998" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="74.6665" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="30.9998" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="74.6665" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="31" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="74.6668" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="31" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="74.6668" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="31" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="74.6668" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="31" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="74.6668" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="31" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="74.6668" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="31" cy="132" r="1.66667" fill="#DADADA" />
              <circle cx="74.6668" cy="132" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="45.6665" cy="132" r="1.66667" fill="#DADADA" />
              <circle cx="89.3333" cy="132" r="1.66667" fill="#DADADA" />
              <circle cx="60.3333" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="1.66679" r="1.66667" fill="#DADADA" />
              <circle cx="60.3333" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="16.3335" r="1.66667" fill="#DADADA" />
              <circle cx="60.3333" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="31.0001" r="1.66667" fill="#DADADA" />
              <circle cx="60.3333" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="45.6668" r="1.66667" fill="#DADADA" />
              <circle cx="60.333" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="60.3335" r="1.66667" fill="#DADADA" />
              <circle cx="60.333" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="88.6668" r="1.66667" fill="#DADADA" />
              <circle cx="60.333" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="117.667" r="1.66667" fill="#DADADA" />
              <circle cx="60.333" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="74.6668" r="1.66667" fill="#DADADA" />
              <circle cx="60.333" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="103" r="1.66667" fill="#DADADA" />
              <circle cx="60.333" cy="132" r="1.66667" fill="#DADADA" />
              <circle cx="104" cy="132" r="1.66667" fill="#DADADA" />
            </svg>
            <img src="assets/images/about/dth.jpg" alt="about" />
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="about-five-content">
            <h6 class="small-title text-lg">OUR STORY</h6>
            <h2 class="main-title fw-bold">Our team comes with the experience and knowledge</h2>
            <div class="about-five-tab">
              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <button class="nav-link active" id="nav-who-tab" data-bs-toggle="tab" data-bs-target="#nav-who" type="button" role="tab" aria-controls="nav-who" aria-selected="true">Who We Are</button>
                  <button class="nav-link" id="nav-vision-tab" data-bs-toggle="tab" data-bs-target="#nav-vision" type="button" role="tab" aria-controls="nav-vision" aria-selected="false">our Vision</button>
                  <button class="nav-link" id="nav-history-tab" data-bs-toggle="tab" data-bs-target="#nav-history" type="button" role="tab" aria-controls="nav-history" aria-selected="false">our History</button>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-who" role="tabpanel" aria-labelledby="nav-who-tab">
                  <p>Desa Tunas Hijau is a new destination for eco-tourism in Labuan. Located at Jalan Sungai Pagar, Kampung Sungai Miri, on the southwest coast of Labuan, this seven-acre site offers facilities and services for everyone, from toddlers to the elderly.</p>
                  <p>We offer outdoor activities for the adventurous, family fun activities, and exquisitely furnished chalets for accommodation. Our large banquet hall can accommodate up to 500 people (dinner set-up) and is suitable for dinners, wedding receptions, large gatherings, and private functions. Its beautiful landscape and luscious greens as well as its night lights will captivate your minds and soul. Serene and secluded, it is fronting the vast South China Sea which offers an amazing and unobstructed view of the sunset.</p>
                </div>
                <div class="tab-pane fade" id="nav-vision" role="tabpanel" aria-labelledby="nav-vision-tab">
                  <p>Desa Tunas Hijau is a new destination for eco-tourism in Labuan. Located at Jalan Sungai Pagar, Kampung Sungai Miri, on the southwest coast of Labuan, this seven-acre site offers facilities and services for everyone, from toddlers to the elderly.</p>
                  <p>We offer outdoor activities for the adventurous, family fun activities, and exquisitely furnished chalets for accommodation. Our large banquet hall can accommodate up to 500 people (dinner set-up) and is suitable for dinners, wedding receptions, large gatherings, and private functions. Its beautiful landscape and luscious greens as well as its night lights will captivate your minds and soul. Serene and secluded, it is fronting the vast South China Sea which offers an amazing and unobstructed view of the sunset.</p>
                </div>
                <div class="tab-pane fade" id="nav-history" role="tabpanel" aria-labelledby="nav-history-tab">
                  <p>Desa Tunas Hijau is a new destination for eco-tourism in Labuan. Located at Jalan Sungai Pagar, Kampung Sungai Miri, on the southwest coast of Labuan, this seven-acre site offers facilities and services for everyone, from toddlers to the elderly.</p>
                  <p>We offer outdoor activities for the adventurous, family fun activities, and exquisitely furnished chalets for accommodation. Our large banquet hall can accommodate up to 500 people (dinner set-up) and is suitable for dinners, wedding receptions, large gatherings, and private functions. Its beautiful landscape and luscious greens as well as its night lights will captivate your minds and soul. Serene and secluded, it is fronting the vast South China Sea which offers an amazing and unobstructed view of the sunset.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- container -->
  </section>

  <!--====== ABOUT FIVE PART ENDS ======-->

  <!-- ===== service-area start ===== -->
  <section id="services" class="services-area services-eight" style="display: none;">
    <!--======  Start Section Title Five ======-->
    <div class="section-title-five">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content">
              <h6>Services</h6>
              <h2 class="fw-bold">Our Best Services</h2>
              <p>
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form.
              </p>
            </div>
          </div>
        </div>
        <!-- row -->
      </div>
      <!-- container -->
    </div>
    <!--======  End Section Title Five ======-->
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <div class="single-services">
            <div class="service-icon">
              <i class="lni lni-capsule"></i>
            </div>
            <div class="service-content">
              <h4>Refreshing Design</h4>
              <p>
                Lorem ipsum dolor sit amet, adipscing elitr, sed diam nonumy
                eirmod tempor ividunt labor dolore magna.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="single-services">
            <div class="service-icon">
              <i class="lni lni-bootstrap"></i>
            </div>
            <div class="service-content">
              <h4>Solid Bootstrap 5</h4>
              <p>
                Lorem ipsum dolor sit amet, adipscing elitr, sed diam nonumy
                eirmod tempor ividunt labor dolore magna.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="single-services">
            <div class="service-icon">
              <i class="lni lni-shortcode"></i>
            </div>
            <div class="service-content">
              <h4>100+ Components</h4>
              <p>
                Lorem ipsum dolor sit amet, adipscing elitr, sed diam nonumy
                eirmod tempor ividunt labor dolore magna.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="single-services">
            <div class="service-icon">
              <i class="lni lni-dashboard"></i>
            </div>
            <div class="service-content">
              <h4>Speed Optimized</h4>
              <p>
                Lorem ipsum dolor sit amet, adipscing elitr, sed diam nonumy
                eirmod tempor ividunt labor dolore magna.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="single-services">
            <div class="service-icon">
              <i class="lni lni-layers"></i>
            </div>
            <div class="service-content">
              <h4>Fully Customizable</h4>
              <p>
                Lorem ipsum dolor sit amet, adipscing elitr, sed diam nonumy
                eirmod tempor ividunt labor dolore magna.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="single-services">
            <div class="service-icon">
              <i class="lni lni-reload"></i>
            </div>
            <div class="service-content">
              <h4>Regular Updates</h4>
              <p>
                Lorem ipsum dolor sit amet, adipscing elitr, sed diam nonumy
                eirmod tempor ividunt labor dolore magna.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ===== service-area end ===== -->


  <!-- Start Pricing  Area -->
  <section id="pricing" class="pricing-area pricing-fourteen" style="display: none;">
    <!--======  Start Section Title Five ======-->
    <div class="section-title-five">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content">
              <h6>Pricing</h6>
              <h2 class="fw-bold">Pricing & Plans</h2>
              <p>
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form.
              </p>
            </div>
          </div>
        </div>
        <!-- row -->
      </div>
      <!-- container -->
    </div>
    <!--======  End Section Title Five ======-->
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-12">
          <div class="pricing-style-fourteen">
            <div class="table-head">
              <h6 class="title">Starter</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                <div class="price">
                  <h2 class="amount">
                    <span class="currency">$</span>0<span class="duration">/mo </span>
                  </h2>
                </div>
            </div>

            <div class="light-rounded-buttons">
              <a href="javascript:void(0)" class="btn primary-btn-outline">
                Start free trial
              </a>
            </div>

            <div class="table-content">
              <ul class="table-list">
                <li> <i class="lni lni-checkmark-circle"></i> Cras justo odio.</li>
                <li> <i class="lni lni-checkmark-circle"></i> Dapibus ac facilisis in.</li>
                <li> <i class="lni lni-checkmark-circle deactive"></i> Morbi leo risus.</li>
                <li> <i class="lni lni-checkmark-circle deactive"></i> Excepteur sint occaecat velit.</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-12">
          <div class="pricing-style-fourteen middle">
            <div class="table-head">
              <h6 class="title">Exclusive</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                <div class="price">
                  <h2 class="amount">
                    <span class="currency">$</span>99<span class="duration">/mo </span>
                  </h2>
                </div>
            </div>

            <div class="light-rounded-buttons">
              <a href="javascript:void(0)" class="btn primary-btn">
                Start free trial
              </a>
            </div>

            <div class="table-content">
              <ul class="table-list">
                <li> <i class="lni lni-checkmark-circle"></i> Cras justo odio.</li>
                <li> <i class="lni lni-checkmark-circle"></i> Dapibus ac facilisis in.</li>
                <li> <i class="lni lni-checkmark-circle"></i> Morbi leo risus.</li>
                <li> <i class="lni lni-checkmark-circle deactive"></i> Excepteur sint occaecat velit.</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-12">
          <div class="pricing-style-fourteen">
            <div class="table-head">
              <h6 class="title">Premium</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                <div class="price">
                  <h2 class="amount">
                    <span class="currency">$</span>150<span class="duration">/mo </span>
                  </h2>
                </div>
            </div>

            <div class="light-rounded-buttons">
              <a href="javascript:void(0)" class="btn primary-btn-outline">
                Start free trial
              </a>
            </div>

            <div class="table-content">
              <ul class="table-list">
                <li> <i class="lni lni-checkmark-circle"></i> Cras justo odio.</li>
                <li> <i class="lni lni-checkmark-circle"></i> Dapibus ac facilisis in.</li>
                <li> <i class="lni lni-checkmark-circle"></i> Morbi leo risus.</li>
                <li> <i class="lni lni-checkmark-circle"></i> Excepteur sint occaecat velit.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ End Pricing  Area -->



  <!-- Start Cta Area -->
  <section id="call-action" class="call-action" style="display: none;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xxl-6 col-xl-7 col-lg-8 col-md-9">
          <div class="inner-content">
            <h2>We love to make perfect <br />solutions for your business</h2>
            <p>
              Why I say old chap that is, spiffing off his nut cor blimey
              guvnords geeza<br />
              bloke knees up bobby, sloshed arse William cack Richard. Bloke
              fanny around chesed of bum bag old lost the pilot say there
              spiffing off his nut.
            </p>
            <div class="light-rounded-buttons">
              <a href="javascript:void(0)" class="btn primary-btn-outline">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Cta Area -->



  <!-- Start Latest News Area -->
  <div id="blog" class="latest-news-area section" style="display: none;">
    <!--======  Start Section Title Five ======-->
    <div class="section-title-five">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content">
              <h6>latest news</h6>
              <h2 class="fw-bold">Latest News & Blog</h2>
              <p>
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form.
              </p>
            </div>
          </div>
        </div>
        <!-- row -->
      </div>
      <!-- container -->
    </div>
    <!--======  End Section Title Five ======-->
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-12">
          <!-- Single News -->
          <div class="single-news">
            <div class="image">
              <a href="javascript:void(0)"><img class="thumb" src="assets/images/blog/1.jpg" alt="Blog" /></a>
              <div class="meta-details">
                <img class="thumb" src="assets/images/blog/b6.jpg" alt="Author" />
                <span>BY TIM NORTON</span>
              </div>
            </div>
            <div class="content-body">
              <h4 class="title">
                <a href="javascript:void(0)"> Make your team a Design driven company </a>
              </h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and
                typesetting industry. Lorem Ipsum has been the industry's
                standard.
              </p>
            </div>
          </div>
          <!-- End Single News -->
        </div>
        <div class="col-lg-4 col-md-6 col-12">
          <!-- Single News -->
          <div class="single-news">
            <div class="image">
              <a href="javascript:void(0)"><img class="thumb" src="assets/images/blog/2.jpg" alt="Blog" /></a>
              <div class="meta-details">
                <img class="thumb" src="assets/images/blog/b6.jpg" alt="Author" />
                <span>BY TIM NORTON</span>
              </div>
            </div>
            <div class="content-body">
              <h4 class="title">
                <a href="javascript:void(0)">
                  The newest web framework that changed the world
                </a>
              </h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and
                typesetting industry. Lorem Ipsum has been the industry's
                standard.
              </p>
            </div>
          </div>
          <!-- End Single News -->
        </div>
        <div class="col-lg-4 col-md-6 col-12">
          <!-- Single News -->
          <div class="single-news">
            <div class="image">
              <a href="javascript:void(0)"><img class="thumb" src="assets/images/blog/3.jpg" alt="Blog" /></a>
              <div class="meta-details">
                <img class="thumb" src="assets/images/blog/b6.jpg" alt="Author" />
                <span>BY TIM NORTON</span>
              </div>
            </div>
            <div class="content-body">
              <h4 class="title">
                <a href="javascript:void(0)">
                  5 ways to improve user retention for your startup
                </a>
              </h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and
                typesetting industry. Lorem Ipsum has been the industry's
                standard.
              </p>
            </div>
          </div>
          <!-- End Single News -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Latest News Area -->

  <!-- Start Brand Area -->
  <div id="clients" class="brand-area section" style="display: none;">
    <!--======  Start Section Title Five ======-->
    <div class="section-title-five">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content">
              <h6>Meet our Clients</h6>
              <h2 class="fw-bold">Our Awesome Clients</h2>
              <p>
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form.
              </p>
            </div>
          </div>
        </div>
        <!-- row -->
      </div>
      <!-- container -->
    </div>
    <!--======  End Section Title Five ======-->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 offset-lg-2 col-12">
          <div class="clients-logos">
            <div class="single-image">
              <img src="assets/images/client-logo/graygrids.svg" alt="Brand Logo Images" />
            </div>
            <div class="single-image">
              <img src="assets/images/client-logo/uideck.svg" alt="Brand Logo Images" />
            </div>
            <div class="single-image">
              <img src="assets/images/client-logo/ayroui.svg" alt="Brand Logo Images" />
            </div>
            <div class="single-image">
              <img src="assets/images/client-logo/lineicons.svg" alt="Brand Logo Images" />
            </div>
            <div class="single-image">
              <img src="assets/images/client-logo/tailwindtemplates.svg" alt="Brand Logo Images" />
            </div>
            <div class="single-image">
              <img src="assets/images/client-logo/ecomhtml.svg" alt="Brand Logo Images" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Brand Area -->

  <!-- ========================= contact-section start ========================= -->
  <section id="contact" class="contact-section" style="display: none;">
    <div class="container">
      <div class="row">
        <div class="col-xl-4">
          <div class="contact-item-wrapper">
            <div class="row">
              <div class="col-12 col-md-6 col-xl-12">
                <div class="contact-item">
                  <div class="contact-icon">
                    <i class="lni lni-phone"></i>
                  </div>
                  <div class="contact-content">
                    <h4>Contact</h4>
                    <p>0984537278623</p>
                    <p>yourmail@gmail.com</p>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 col-xl-12">
                <div class="contact-item">
                  <div class="contact-icon">
                    <i class="lni lni-map-marker"></i>
                  </div>
                  <div class="contact-content">
                    <h4>Address</h4>
                    <p>175 5th Ave, New York, NY 10010</p>
                    <p>United States</p>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 col-xl-12">
                <div class="contact-item">
                  <div class="contact-icon">
                    <i class="lni lni-alarm-clock"></i>
                  </div>
                  <div class="contact-content">
                    <h4>Schedule</h4>
                    <p>24 Hours / 7 Days Open</p>
                    <p>Office time: 10 AM - 5:30 PM</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8">
          <div class="contact-form-wrapper">
            <div class="row">
              <div class="col-xl-10 col-lg-8 mx-auto">
                <div class="section-title text-center">
                  <span> Get in Touch </span>
                  <h2>
                    Ready to Get Started
                  </h2>
                  <p>
                    At vero eos et accusamus et iusto odio dignissimos ducimus
                    quiblanditiis praesentium
                  </p>
                </div>
              </div>
            </div>
            <form action="#" class="contact-form">
              <div class="row">
                <div class="col-md-6">
                  <input type="text" name="name" id="name" placeholder="Name" required />
                </div>
                <div class="col-md-6">
                  <input type="email" name="email" id="email" placeholder="Email" required />
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <input type="text" name="phone" id="phone" placeholder="Phone" required />
                </div>
                <div class="col-md-6">
                  <input type="text" name="subject" id="email" placeholder="Subject" required />
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <textarea name="message" id="message" placeholder="Type Message" rows="5"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="button text-center rounded-buttons">
                    <button type="submit" class="btn primary-btn rounded-full">
                      Send Message
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ========================= contact-section end ========================= -->

  <!-- ========================= map-section end ========================= -->
  <!-- <section class="map-section map-style-9">
    <div class="map-container">
      <object style="border:0; height: 500px; width: 100%;"
        data="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3102.7887109309127!2d-77.44196278417968!3d38.95165507956235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzjCsDU3JzA2LjAiTiA3N8KwMjYnMjMuMiJX!5e0!3m2!1sen!2sbd!4v1545420879707"></object>
    </div>
    </div>
  </section> -->
  <!-- ========================= map-section end ========================= -->

  <!-- Button trigger modal -->

  <!-- Start Footer Area -->
  <footer class="footer-area footer-eleven">
    <!-- Start Footer Top -->
    <div class="footer-top">
      <div class="container">
        <div class="inner-content">
          <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
              <!-- Single Widget -->
              <div class="footer-widget f-about">
                <div class="logo">
                  <a href="index.html">
                    <img src="assets/images/logo.jpg" alt="#" class="img-fluid" style="width: 120px;" />
                  </a>
                </div>
                <p>
                  Jalan Sg Pagar, Kg Sg Miri, P.O. Box 81858, 87008, W.P. Labuan.
                </p>
                <p class="copyright-text">
                  <span>© 2024 Desa Tunas Hijau.</span>
                  <!-- <a href="javascript:void(0)" rel="nofollow"> Ayro UI </a> -->
                </p>
              </div>
              <!-- End Single Widget -->
            </div>
            <div class="col-lg-2 col-md-6 col-12">
              <!-- Single Widget -->
              <div class="footer-widget f-link">
                <h5>Solutions</h5>
                <ul>
                  <li><a href="javascript:void(0)">Marketing</a></li>
                  <li><a href="javascript:void(0)">Analytics</a></li>
                  <li><a href="javascript:void(0)">Commerce</a></li>
                  <li><a href="javascript:void(0)">Insights</a></li>
                </ul>
              </div>
              <!-- End Single Widget -->
            </div>
            <div class="col-lg-2 col-md-6 col-12">
              <!-- Single Widget -->
              <div class="footer-widget f-link">
                <h5>Support</h5>
                <ul>
                  <li><a href="javascript:void(0)">Pricing</a></li>
                  <li><a href="javascript:void(0)">Documentation</a></li>
                  <li><a href="javascript:void(0)">Guides</a></li>
                  <li><a href="javascript:void(0)">API Status</a></li>
                </ul>
              </div>
              <!-- End Single Widget -->
            </div>
            <div class="col-lg-4 col-md-6 col-12">
              <!-- Single Widget -->
              <div class="footer-widget newsletter">
                <h5>Subscribe</h5>
                <p>Subscribe to our newsletter for the latest updates</p>
                <form action="#" method="get" target="_blank" class="newsletter-form">
                  <input name="EMAIL" placeholder="Email address" required="required" type="email" />
                  <div class="button">
                    <button class="sub-btn">
                      <i class="lni lni-envelope"></i>
                    </button>
                  </div>
                </form>
              </div>
              <!-- End Single Widget -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ End Footer Top -->
  </footer>
  <!--/ End Footer Area -->

  <!-- <div class="made-in-ayroui mt-4">
		<a href="https://ayroui.com" target="_blank" rel="nofollow">
		  <img style="width:220px" src="assets/images/ayroui.svg">
		</a>
	</div> -->

  <a href="#" class="scroll-top btn-hover">
    <i class="lni lni-chevron-up"></i>
  </a>

  <!--====== js ======-->
  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/glightbox.min.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/tiny-slider.js"></script>

  <!-- Added -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script src="assets/js/my-script.js"></script>

  <script>
    //===== close navbar-collapse when a  clicked
    let navbarTogglerNine = document.querySelector(
      ".navbar-nine .navbar-toggler"
    );
    navbarTogglerNine.addEventListener("click", function() {
      navbarTogglerNine.classList.toggle("active");
    });

    // ==== left sidebar toggle
    let sidebarLeft = document.querySelector(".sidebar-left");
    let overlayLeft = document.querySelector(".overlay-left");
    let sidebarClose = document.querySelector(".sidebar-close .close");

    overlayLeft.addEventListener("click", function() {
      sidebarLeft.classList.toggle("open");
      overlayLeft.classList.toggle("open");
    });
    sidebarClose.addEventListener("click", function() {
      sidebarLeft.classList.remove("open");
      overlayLeft.classList.remove("open");
    });

    // ===== navbar nine sideMenu
    let sideMenuLeftNine = document.querySelector(".navbar-nine .menu-bar");

    sideMenuLeftNine.addEventListener("click", function() {
      sidebarLeft.classList.add("open");
      overlayLeft.classList.add("open");
    });

    //========= glightbox
    // GLightbox({
    //   'href': 'https://www.youtube.com/watch?v=r44RKWyfcFw&fbclid=IwAR21beSJORalzmzokxDRcGfkZA1AtRTE__l5N4r09HcGS5Y6vOluyouM9EM',
    //   'type': 'video',
    //   'source': 'youtube', //vimeo, youtube or local
    //   'width': 900,
    //   'autoplayVideos': true,
    // });
  </script>
</body>

</html>