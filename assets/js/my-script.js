$(document).ready(function () {
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true
    });

    $('.timepicker').timepicker({
        zindex: 9999999,
        timeFormat: 'h:mm p',
        interval: 1,
        minTime: '01',
        maxTime: '23:59pm',
        defaultTime: '08',
        startTime: '01:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
});